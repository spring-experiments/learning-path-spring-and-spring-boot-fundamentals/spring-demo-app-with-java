package org.elu.learn.spring.springdemoappwithjava.services;

import org.elu.learn.spring.springdemoappwithjava.json.AstroResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest
class AstroServiceTest {
    @Autowired
    private AstroService service;

    @Test
    void getAstronautsRT() {
        final var result = service.getAstronautsRT();

        checkResult(result);
    }

    @Test
    void getAstronautsWC() {
        final var result = service.getAstronautsWC();

        checkResult(result);
    }

    private void checkResult(final AstroResult result) {
        assertThat(result).isNotNull();
        final int number = result.number();
        System.out.printf("There are %d people in space%n", number);
        final var people = result.people();
        people.forEach(System.out::println);
        assertAll(
                () -> assertThat(number).isGreaterThanOrEqualTo(0),
                () -> assertThat(people.size()).isEqualTo(number)
        );
    }
}
