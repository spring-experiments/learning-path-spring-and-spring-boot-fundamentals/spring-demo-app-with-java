package org.elu.learn.spring.springdemoappwithjava;

import org.elu.learn.spring.springdemoappwithjava.json.Greeting;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
class SpringDemoAppWithJavaApplicationTests {
    @Autowired
    private ApplicationContext context;

    @Test
    void contextLoads() {
        assertThat(context).isNotNull();
        System.out.println(context.getClass().getName());
        final int count = context.getBeanDefinitionCount();
        System.out.printf("There are %s beans in the application context%n", count);
        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
    }

    @Test
    void twoGreetingBeanInAppCtx() {
        assertThatThrownBy(() -> context.getBean(Greeting.class))
                .isInstanceOf(NoUniqueBeanDefinitionException.class);
    }

    @Test
    void getBeanTwice() {
        final var greeting1 = context.getBean("defaultGreeting", Greeting.class);
        final var greeting2 = context.getBean("defaultGreeting", Greeting.class);
        assertThat(greeting1).isSameAs(greeting2);
        System.out.println(greeting1);
    }
}
