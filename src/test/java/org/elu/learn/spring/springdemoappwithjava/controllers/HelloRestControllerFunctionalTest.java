package org.elu.learn.spring.springdemoappwithjava.controllers;

import org.elu.learn.spring.springdemoappwithjava.json.Greeting;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HelloRestControllerFunctionalTest {
    @Autowired
    private TestRestTemplate template;

    @Test
    void testGreetWithName() {
        final var response = template.getForObject("/rest?name=Janna", Greeting.class);

        assertThat(response.message()).isEqualTo("Hello, Janna!");
    }

    @Test
    void testGreetWithoutName() {
//    void testGreetWithoutName(@Autowired final TestRestTemplate template) { // it is possible to pass autowired dependency as test method parameter
        final ResponseEntity<Greeting> response = template.getForEntity("/rest", Greeting.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isNotNull();
        final var greeting = response.getBody();
        assertThat(greeting.message()).isEqualTo("Hello, World!");
    }
}
