package org.elu.learn.spring.springdemoappwithjava.services;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.withPrecision;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest
class GeocoderServiceTest {
    private final Logger logger = LoggerFactory.getLogger(GeocoderServiceTest.class);

    @Autowired
    private GeocoderService service;

    @Test
    void getLatLngWithoutStreet() {
        final var site = service.getLatLng("Boston", "MA");

        assertThat(site).isNotNull();
        logger.info(site.toString());
        assertAll(
                () -> assertThat(site.latitude()).isEqualTo(42.36, withPrecision(0.01)),
                () -> assertThat(site.longitude()).isEqualTo(-71.06, withPrecision(0.01))
        );
    }

    @Test
    void getLatLngWithStreet() {
        final var site = service.getLatLng("1600 Amphitheatre Parkway", "Mountain View", "CA");

        assertThat(site).isNotNull();
        logger.info(site.toString());
        assertAll(
                () -> assertThat(site.latitude()).isEqualTo(37.42, withPrecision(0.01)),
                () -> assertThat(site.longitude()).isEqualTo(-122.08, withPrecision(0.01))
        );
    }
}
