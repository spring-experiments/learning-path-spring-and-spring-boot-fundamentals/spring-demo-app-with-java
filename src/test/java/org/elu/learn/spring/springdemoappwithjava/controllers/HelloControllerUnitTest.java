package org.elu.learn.spring.springdemoappwithjava.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class HelloControllerUnitTest {
    @Test
    void testSayHello() {
        final var name = "Janna";
        final var controller = new HelloController();
        final var model = new BindingAwareModelMap();

        final var result = controller.sayHello(name, model);

        assertAll(
                () -> assertThat(model.getAttribute("user")).isEqualTo(name),
                () -> assertThat(result).isEqualTo("hello")
        );
    }

    @Test
    void sayHelloWithStub() { // this is a real unit test
        final var name = "Janna";
        final var controller = new HelloController();
        final var mockModel = mock(Model.class);
        // set expectations on mock
        when(mockModel.addAttribute(anyString(), any())).thenReturn(mockModel);

        final var result = controller.sayHello(name, mockModel);

        assertThat(result).isEqualTo("hello");
    }
}
