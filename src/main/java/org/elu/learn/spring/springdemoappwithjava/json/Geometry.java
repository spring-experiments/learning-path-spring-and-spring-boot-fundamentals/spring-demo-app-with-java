package org.elu.learn.spring.springdemoappwithjava.json;

public record Geometry(Location location) {
}
