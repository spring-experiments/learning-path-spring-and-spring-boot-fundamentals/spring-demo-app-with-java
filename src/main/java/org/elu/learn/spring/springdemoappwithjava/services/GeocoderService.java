package org.elu.learn.spring.springdemoappwithjava.services;

import org.elu.learn.spring.springdemoappwithjava.entities.Site;
import org.elu.learn.spring.springdemoappwithjava.json.Response;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GeocoderService {
    private final static String KEY = "<your google key>";
    private final static String BASE_URL = "https://maps.googleapis.com";
    private final WebClient client;

    public GeocoderService(WebClient.Builder builder) {
        this.client = builder.baseUrl(BASE_URL).build();
    }

    public Site getLatLng(String... address) {
        final var encoded = Stream.of(address)
                .map(s -> URLEncoder.encode(s, StandardCharsets.UTF_8))
                .collect(Collectors.joining(", "));
        final var path = "/maps/api/geocode/json";
        final var response = client.get()
                .uri(uriBuilder -> uriBuilder.path(path)
                        .queryParam("address", encoded)
                        .queryParam("key", KEY)
                        .build())
                .retrieve()
                .bodyToMono(Response.class)
                .block(Duration.ofSeconds(2));
        if (response == null || !Objects.equals(response.status(), "OK")) {
            return null;
        }
        return new Site(null, response.getFormattedAddress(), response.getLocation().lat(),
                response.getLocation().lng());
    }
}
