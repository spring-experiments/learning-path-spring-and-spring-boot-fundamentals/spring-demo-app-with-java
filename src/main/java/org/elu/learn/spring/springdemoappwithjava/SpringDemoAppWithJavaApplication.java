package org.elu.learn.spring.springdemoappwithjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDemoAppWithJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoAppWithJavaApplication.class, args);
    }

}
