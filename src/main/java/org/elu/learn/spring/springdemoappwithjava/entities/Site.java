package org.elu.learn.spring.springdemoappwithjava.entities;

public record Site(Integer id, String address, double latitude, double longitude) {
}
