package org.elu.learn.spring.springdemoappwithjava.json;

public record Assignment(String name, String craft) {
}
