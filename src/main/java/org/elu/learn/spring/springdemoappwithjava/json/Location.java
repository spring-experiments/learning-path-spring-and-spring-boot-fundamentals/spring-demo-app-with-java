package org.elu.learn.spring.springdemoappwithjava.json;

public record Location(double lat, double lng) {
}
