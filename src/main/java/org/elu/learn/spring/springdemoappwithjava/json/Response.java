package org.elu.learn.spring.springdemoappwithjava.json;

import java.util.List;

public record Response(List<Result> results, String status) {
    public String getFormattedAddress() {
        return results.get(0).formattedAddress();
    }

    public Location getLocation() {
        return results.get(0).geometry().location();
    }
}
