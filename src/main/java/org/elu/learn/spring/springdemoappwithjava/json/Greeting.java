package org.elu.learn.spring.springdemoappwithjava.json;

public record Greeting(String message) {
}
